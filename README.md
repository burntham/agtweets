Requires Java 8
Developed Using IntelliJ community Edition

The file can be executed by running the AGTweets.jar file from command line as:
java -jar AGTweets.jar <userfile> <tweetfile>

The program automatically looks for user.txt and tweet.txt if it is not executed with arguments.

Assumptions:
Files are always 7bit ASCII.
Files may contain invalid data
	- Tweets that are too long, etc
	- Invalid/formatted User/followers text files
Users may follow no one.
Text files are in the same directory as the executable
Nothing is displayed if one of the files could not be found.
Tweets with more than 140 characters are considered invalid and ignored
Information related the processing of

Suggestions for future improvement:
	- Store Tweets and users in a database for persistance.