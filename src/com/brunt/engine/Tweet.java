package com.brunt.engine;

/**
 * Created by Daniel on 03-Jul-16.
 * Tweet stores the message and original sender
 */
public class Tweet {
    User sender;
    String message;

    public Tweet(User sender, String message){
        this.sender = sender;
        this.message = message;
    }
}
