package com.brunt.engine;

import java.util.*;

/**
 * Created by Daniel on 03-Jul-16.
 */
public class User {
   private HashSet<String> followers;
   private ArrayList<Tweet> tweets;
   private String userName;


    public User(String name){
        followers = new HashSet<String>();
        tweets = new ArrayList<Tweet>();
        userName = name;
        //The user follows himself
        followers.add(userName);
    }

    public void addTweet(Tweet t){
        tweets.add(t);
    }

    public String getUserName()
    {
        return userName;
    }

    public void addFollower(String follower){
        followers.add(follower);
    }

    public ArrayList<Tweet> getTweets(){
        return tweets;
    }

    public List<String> getFollowers(){
        ArrayList<String> followerList = new ArrayList<String>();
        for (String f : followers){
            followerList.add(f);
        }
        return followerList;
    }
}
