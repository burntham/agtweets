package com.brunt.engine;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Daniel on 03-Jul-16.
 * TweetEngine manages users and tweets
 * Uses singleton pattern
 */
public class TweetEngine {
    HashMap<String, User> userMap;

    private static TweetEngine engineInstance;

    private TweetEngine(){
        //Initilize a new user network
        userMap = new HashMap<String, User>();
    }

    /**
     * Called if one of the files fails to process
     */
    public void resetEngine(){
        userMap.clear();
    }

    public static TweetEngine getEngineSingleton(){
        if(engineInstance == null)
            return engineInstance = new TweetEngine();
        else
            return engineInstance;
    }

    /**
     * Create a new user, and add any followee's as users too
     */
    public void processUser(String follower, String[] followees){
        User newUser = userMap.get(follower);

        //Do not create a new user if they already exist
        if(newUser==null){
            userMap.put(follower, newUser = new User(follower));
        }

        if (followees != null) {
            for (String f : followees) {
                User followee = userMap.get(f);
                if (followee == null) {
                    followee = new User(f);
                    userMap.put(f, followee);
                }
                followee.addFollower(follower);
            }
        }
    }

    /**
     * process the tweet, and post to each followers user profile
     * @param tweeter
     * @param message
     */
    public void processTweet(String tweeter, String message){
        User user = userMap.get(tweeter);
        if(user == null){
            System.out.printf("Error, user %s not found, message cannot be tweeted\n", tweeter);
            return;
        }

        Tweet newTweet = new Tweet(user, message);
        //Post to each user
        for (String follower : user.getFollowers()){
            userMap.get(follower).addTweet(newTweet);
        }
    }

    public void printTweets(){
        System.out.println("\n-----Displaying tweets below-----\n");
        String[] sortedUserList = new String[userMap.size()];
        Arrays.sort(userMap.keySet().toArray(sortedUserList));

        for (String tweeter : sortedUserList){
            System.out.println(tweeter);
            for(Tweet tweet : userMap.get(tweeter).getTweets()){
                System.out.printf("\t@%s: %s\n",tweet.sender.getUserName(), tweet.message);
            }
        }
    }
}