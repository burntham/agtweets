package com.brunt;

/**
 * Created by Daniel on 03-Jul-16.
 */
public interface Store {
    public String[] getAll();

    public void add(String line);
}
