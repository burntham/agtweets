package com.brunt;

import com.brunt.engine.TweetEngine;
import com.brunt.io.FileHandler;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TweetEngine te = TweetEngine.getEngineSingleton();
        boolean filesProcessed = false;
        String userFile, tweetFile;

        if(args.length!=2){
            userFile="user.txt";
            tweetFile = "tweet.txt";

        }else{
            userFile = args[0];
            tweetFile = args[1];
        }

        Scanner input = new Scanner(System.in);

        FileHandler FH = new FileHandler();

        while((filesProcessed = FH.processFiles(userFile, tweetFile))==false ){
            System.out.print("Please enter your user file name: ");
            userFile = input.nextLine();
            System.out.print("Please enter your tweet file name: ");
            tweetFile = input.nextLine();
        }
        te.printTweets();
    }
}