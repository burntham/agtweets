package com.brunt.io;
import com.brunt.engine.TweetEngine;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Daniel on 03-Jul-16.
 * FileHandler class is responsible for reading the user and tweet files
 */
public class FileHandler {
    File userFile, tweetFile;
    TweetEngine tweetEngine;

    Pattern userPattern = Pattern.compile("^(?<user>\\S*)\\s*((follows)\\s*(?<followees>.*))?$");
    Pattern tweetPattern = Pattern.compile("(?<user>[^> ]+)(> )(?<message>.{1,140})$");

    public FileHandler() {
        tweetEngine = TweetEngine.getEngineSingleton();
    }

    public boolean processFiles(String userFilePath, String tweetFilePath) {
        userFile = new File(userFilePath);
        tweetFile = new File(tweetFilePath);

        try {
            processUserFile(userFile);
            processTweetFile(tweetFile);
        } catch (FileNotFoundException e) {
            System.out.printf("File not found: \"%s\"\n", e.getMessage().split(" ")[0]);
            tweetEngine.resetEngine();
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private void processUserFile(File inputFile) throws IOException {
        FileReader fr = new FileReader(inputFile);
        BufferedReader br = new BufferedReader(fr);

        String line;

        while ((line = br.readLine()) != null) {
            Matcher m = userPattern.matcher(line);
            if(m.matches()){
                String[] followeeArr = null;
                String followees = m.group("followees");
                if(followees!=null){
                    followeeArr = followees.split("\\s*,?\\s+");
                }

                tweetEngine.processUser(m.group("user"),followeeArr);
            }else {
                System.out.printf("Invalid User line: %s\n", line);
            }
        }
        fr.close();
        System.out.printf("Processed file: %s\n", inputFile.toString());
    }

    private void processTweetFile(File inputFile) throws IOException {
        FileReader fr = new FileReader(inputFile);
        BufferedReader br = new BufferedReader(fr);

        String line;

        while ((line = br.readLine()) != null) {
            Matcher m = tweetPattern.matcher(line);
            if(m.matches()){
                tweetEngine.processTweet(m.group("user"), m.group("message"));
            }
            else{
                System.out.printf("Invalid tweet detected, ignored line: %s\n", line);
            }
        }
        fr.close();
        System.out.printf("Processed file: %s\n", inputFile.toString());
    }
}
